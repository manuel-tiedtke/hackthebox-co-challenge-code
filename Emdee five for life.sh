#!/bin/bash

# Colors ##########################################################################
CSI="\\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CYELLOW="${CSI}1;33m"
CGREEN="${CSI}1;32m"
URL="http://docker.hackthebox.eu:30295/"

# TODO: read URL from ARGs

cookie_file=$(mktemp /tmp/embdee_five_for_live.cookies.XXXXXX)
# Request the website and parse the string that has to be hashed with MD5 from the response.
echo -e "${CGREEN}Requesting website... extracting string...${CEND}"
input_string=$(curl -s -c $cookie_file $URL | grep "<h3 align" | cut -d ">" -f4 | cut -d "<" -f1)

# Hash the given string from the website with MD5, so we can send it back with a POST request.
echo -e "${CGREEN}Hashing string with MD5...${CEND}"
md5_string=$(echo $input_string | md5sum | cut -d ' ' -f1)

# DEBUG MESSAGES
echo -e "${CYELLOW}String${CEND}: $input_string"
echo -e "${CYELLOW}MD5-String${CEND}: $md5_string"
echo -e "${CYELLOW}Cookies${CEND}: $(cat $cookie_file | tail -n1)"

# Send hash back with POST request.
curl -b $cookie_file -X POST -F "hash=${md5_string}" $URL

rm $cookie_file